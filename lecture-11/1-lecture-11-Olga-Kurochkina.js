const lookupoObject = { S: 8, A: 6, B: 4, C: 3, D: 2, F: 0 };
function calculateTotalScore(string) {
    let sum = 0;
    for (i = 0; i < string.length; i++) {
        letter = string.charAt(i);
        sum += lookupoObject[letter];
    }
    return sum;
}
const totalScore = calculateTotalScore("DFCBDABSB");
console.log("a) " + totalScore); // prints 33

function calculateAverageScore(string) {
    return (calculateTotalScore(string) / string.length).toFixed(3);
}
const averageScore = calculateAverageScore("DFCBDABSB");
console.log("b) " + averageScore); // prints 3.6666666666666665

const array = ["AABAACAA", "FFDFDCCDCB", "ACBSABA", "CCDFABABC"];
const newArray = array.map(x => {
    return calculateAverageScore(x);
});
console.log("c) " + newArray);
