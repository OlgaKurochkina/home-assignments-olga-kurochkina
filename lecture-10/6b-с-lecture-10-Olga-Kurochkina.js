const objectArray = [ { x: 14, y: 21, type: "tree", toDelete: false },
                      { x: 1, y: 30, type: "house", toDelete: false },
                      { x: 22, y: 10, type: "tree", toDelete: true },
                      { x: 5, y: 34, type: "rock", toDelete: true },
                      null,
                      { x: 19, y: 40, type: "tree", toDelete: false },
                      { x: 35, y: 35, type: "house", toDelete: false },
                      { x: 19, y: 40, type: "tree", toDelete: true },
                      { x: 24, y: 31, type: "rock", toDelete: false } ];

const newArray= objectArray.map(object=>{
    if ((!(object===null))&&(object.toDelete==true)){
        return null} else {
        return object;
    }
});
console.log(newArray);

/* с)
Typically an array would be a continuous range in memory - making an array bigger
 generally involves copying the whole array to a new memory location, sufficient 
 in size. This is a very costly operation. So it makes more sense to create a new 
 array. */