const lookupObject = {
    hello: "hei",
    world: "maailma",
    bit: "bitti",
    byte: "tavu",
    integer: "kokonaisluku",
    boolean: "totuusarvo",
    string: "merkkijono",
    network: "verkko"
};
function printTranslatableWords() {
    console.log("" + Object.keys(lookupObject));
}
console.log("b)");
printTranslatableWords();

function translate(word) {
    if (word in lookupObject) {
        return lookupObject[word];
    } else {
        console.log("No translation exists for word ");
        return null;
    }
}
console.log("c)");
console.log(translate("network")); // prints "verkko"

console.log("d)");
console.log(translate("cat"));


