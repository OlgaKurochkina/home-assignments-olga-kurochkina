// Assignment 1 (Fix code syntax)

const userName="Juliet";
/*this is a comment
we greet the user*/
console.log("Hello", userName);

//Assignment 2 (Fix more code syntax)

// First print 5 and then print 7
let number = 5;
console.log (number);
number = 7;
console.log (number);

//Assignment 3 (Printing variables)
// a)

const month=10;
const year=2022;
console.log (month, year);

// b)

const name="Juliet";
const age=17;
console.log (name + " is " + age + " years old");

//Assignment 4 (Sum)

const price=950;
const increaseOfPrice=45;
result=price+increaseOfPrice;
console.log ("Original price ", price);
console.log ("Increase ", increaseOfPrice);
console.log ("Final price", result);
console.log (price+" + "+increaseOfPrice+" = "+result);

//Assignment 5 (Code cleanup)

let value = 2;
//console.log(value);
const increase = 3;
//console.log(increase);
value = value + increase; //value=2+3=5
//console.log(value);
//console.log(increase);
const newValue = value + increase;  //newValue=5+3=8
//console.log(newValue);
//console.log(increase);
value = value + increase; //value=5+3=8
const increaseMinusOne = increase - 1; //increaseMinusOne=2
value = value + increaseMinusOne; // value=8+2=10
console.log("result: " + value);
//console.log(increaseMinusOne);

//EXTRA: Assignment 6 (More code fixing)


let number6 = 6;
const increase6 = 4;
let limit = 11;
number6 = number6 + increase6;

/* Print number if it is 
bigger 
than limit (11) */
if (number6 > limit) 
 {
     console.log(number6);
}
number6 = number6 + increase6;
// Check if it is bigger after it has been increased again
if (number6 > limit) 
 { 
    console.log(number6);
}