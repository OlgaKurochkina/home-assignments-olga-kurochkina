function days(i){
    const months=["January","February","March","April","May","June", 
    "July","August","September","October","November","December"];
    const daysInMonth=[31,28,31,30,31,30,31,31,30,31,30,31];
    if ((i<=12)&&(i>=0)){
        i--;
        month=months[i];
        days=daysInMonth[i];
        sentence="There are "+days+" days in "+month;
        return sentence;
    } else {
        sentence="Unknown month"
    }
}
days(3);
console.log(sentence);
