import "./style.css";
const Planets = (props) => {
  
    return (
      
      <div>
          <table>
        {props.planetList.map((planetList) =>
          
            <tr>
                <td key={planetList.name}>
              {planetList.name}
              </td>
              <td key={planetList.climate}>
              {planetList.climate}
              </td>
            </tr>
       
       
        )}
        </table>
      </div>
    )
   }

export default Planets