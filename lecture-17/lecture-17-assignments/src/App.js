import Greeting from './Greeting'
import Planets from './Planets'
import Assignment2 from './Assignment2'

const App = () => {
  const fullName = 'Mikki';
  const age1=18;
  const planetList = [
    { name: "Hoth", climate: "Ice" },
    { name: "Tattooine", climate: "Desert" },
    { name: "Alderaan", climate: "Temperate" },
    { name: "Mustafar", climate: "Volcanic", }
  ]
  
  return (
    <div>
      <h1>Assignment 1</h1><br/>
      <Greeting name={fullName} age={age1}  /><br/>
      <h1>Assignment 2</h1><br/>
      <Assignment2   /><br/>
      <h1>Assignment 3</h1><br/>
      <Planets planetList={planetList} />
    </div>
  )
}
export default App


