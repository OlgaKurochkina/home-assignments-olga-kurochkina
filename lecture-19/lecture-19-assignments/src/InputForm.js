import { useState } from "react"

const InputForm = (props) => {
  console.log(props);
const [newTodo, setNewTodo]=useState('');
const handleSubmit = event => {
    event.preventDefault();
    props.onSubmit(newTodo);
  };

return ( 
  <>
  <div>
    <form onSubmit={handleSubmit}>
        <fieldset>
			<legend>Add a new note</legend>			     
			<input
				value = {newTodo}
				onChange = {(event) => {
					setNewTodo(event.target.value);
				}}
			/>
            <button type="submit">Submit</button>
           </fieldset>
    </form>
  </div>
  </>
)
}	
    

export default InputForm
