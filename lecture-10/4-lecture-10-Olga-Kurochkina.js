function incrementAll(numbers) {
    const incrementArray = numbers.map(number => number + 1);
    return incrementArray;
}
function decrementAll(numbers) {
    const decrementArray = numbers.map(number => number - 1);
    return decrementArray;
}

const numbers = [4, 7, 1, 8, 5];
console.log(numbers);

const newNumbers = incrementAll(numbers);
console.log(newNumbers); // prints [ 5, 8, 2, 9, 6 ]

const newNumbers2 = decrementAll(numbers);
console.log(newNumbers2); // prints [ 3, 6, 0, 7, 4 ]

