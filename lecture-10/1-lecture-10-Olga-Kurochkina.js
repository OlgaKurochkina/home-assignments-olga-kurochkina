const array = [8, 12, 17, 9, 16, 24, 16, 25, 35, 27, 38, 50];
// a)
for (i = 0; i < array.length; i++) {
    if (array[i] > 20) {
        console.log("a) " + array[i]);
        break;
    }
}
// b)
const result = array.find(number => number > 20);
console.log("b) " + result);

// c)
const index = array.findIndex(number => number > 20);
console.log("c) " + index);
// d)
array.splice(index + 1);
console.log("d) " + array);