function findLargest(array){
    let i=0;
    let number=array[i];
    for (const i of array){
        if (number<=i) {number=i}
    }
    return number;
}
const array = [4, 19, 7, 1, 9, 22, 6, 13];
const largest = findLargest(array);
console.log("The largest number in a given array of numbers is "+largest); // prints 22
