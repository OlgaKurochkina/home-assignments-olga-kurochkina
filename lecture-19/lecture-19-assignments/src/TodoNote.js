import "./style.css"
import { useState } from "react"
const TodoNote = (props) => {
    const [string, setString] = useState(props.todo.text);
    const [output, setOutput] = useState(string);
    const onStringChange = (event) => {
        setString(event.target.value)
    }
    const onOutputChange = (event) => {
        setOutput(string);
        console.log(output);
        props.onChange (props.index, output);
        setEditMode(false);
    }
    const [editMode, setEditMode]=useState(false);
    const onClickEdit = () => {
          setEditMode(true);
      };
    return (
            <>
            <div className={
			(!props.todo.complete)
			? "todo-note pink"
			: "todo-note"
		}>
  { !editMode && <  input
					          type="checkbox"
					          checked = {props.todo.complete}
					          onChange = {() => {
                        props.onCompletionToggle()
                    }} />}
            {(editMode===false)&&<p>{props.todo.text}</p>}
            {editMode&&<input
				value={string}
                onChange={onStringChange}
			/>
            }
            {(editMode===false)&&<button className="button"  onClick={onClickEdit}>Edit</button>}
            {editMode&&<button  onClick={onOutputChange}>Save</button>}
            <button  onClick = {props.onRemoveClick}>x</button>
            </div>
            </>
        )
}	
export default TodoNote
