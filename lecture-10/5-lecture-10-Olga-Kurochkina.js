const students = [{ name: "Sami", score: 24.75 },
{ name: "Heidi", score: 20.25 },
{ name: "Jyrki", score: 27.5 },
{ name: "Helinä", score: 26.0 },
{ name: "Maria", score: 17.0 },
{ name: "Yrjö", score: 14.5 }];
function getGrades(students) {
    let grades = [];
    students.forEach(student => {
        if (student.score < 14.0) {
            grades.push({name:student.name, grade:0}) };;
        if ((student.score >= 14) && (student.score <= 17)) {
            grades.push({name:student.name, grade:1}) };
        if ((student.score > 17) && (student.score <= 20)) {
            grades.push({name:student.name, grade:2}) };
        if ((student.score > 20) && (student.score <= 23)) {
            grades.push({name:student.name, grade:3}) };
        if ((student.score > 23) && (student.score <= 26)) {
            grades.push({name:student.name, grade:4}) };
        if (student.score > 26) {
            grades.push({name:student.name, grade:5}) };
    });
    return grades;
}
result=getGrades(students)
console.log(result);