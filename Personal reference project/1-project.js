//B1.1
const fs = require('fs');
const prompt = require('prompt-sync')();

const data = fs.readFileSync("books.json", "utf8");
let books = JSON.parse(data); 
const alteredData=JSON.stringify(books);
//B1.2
fs.writeFileSync("books.json", alteredData,"utf8", (err) => {
    if (err) {
        console.log("Could not save books to file!");
    }
});
//B1.3

let currentBook=[];
function getBookByISBN() {
    let isbn = prompt('ISBN is ');
    let a=0;
    books.forEach(book => {
        if (book.isbn===isbn){
            a=isbn;
            currentBook=book;
        } 
    });
    if (a===0) {
        console.log("no book found with this ISBN");  
    } 
}


function getBookByAuthorTitle() {
    let author = prompt('Author is ');
    let title = prompt('Title is ');
    let a=0;
    books.forEach(book => {
        if ((book.author===author)&&(book.title===title)){
            a=title;
            currentBook=book;
        }  
    });
    if (a===0) {
        console.log("no book found with this title and author");  
    }   
}


//B1.4+B1.5
function printBookDetails(currentBook){
    console.log(`${currentBook.title} by ${currentBook.author} (${currentBook.published.substring(0, 4)})`);
    console.log(`Books in library: ${currentBook.copies.length}`);
    numberOfAvailableBooks=0;
    currentBook.copies.forEach(copy => {
        if (copy.status==="in_library"){
            numberOfAvailableBooks++;
        }
    });
    console.log(`Available for borrowing: ${numberOfAvailableBooks}`); 
}


//B2.3
let userID=0;
function getRandom() {
    return userID=""+Math.floor(Math.random() * 100000000);
    }

//B2.1

const dataUsers = fs.readFileSync("users.json", "utf8");
let users = JSON.parse(dataUsers); 

//B2.2


//F0.1
console.log("Welcome to Varpaisjärvi library!");
console.log("Get the list of available commands by typing 'help'");
//F0.2
function printHelp(){
    console.log("Here’s a list of commands you can use!");
    console.log("\n");
    console.log("AVAILABLE EVERYWHERE");
    console.log("help                Prints this listing.");
    console.log("quit                Quits the program.");
    console.log("search              Opens a dialog for searching for a book in the library system.");
    console.log("\n");
    console.log("ONLY AVAILABLE WHEN LOGGED OUT");
    console.log("signup              Opens a dialog for creating a new account.");
    console.log("login               Opens a dialog for logging into an existing account.");
    console.log("\n");
    console.log("ONLY AVAILABLE WHEN LOGGED IN");
    console.log("list                Lists the books you are currently borrowing.");
    console.log("borrow              Opens a dialog for borrowing a book.");
    console.log("return              Opens a dialog for returning a book.");
    console.log("change_name         Opens a dialog for changing the name associated with the account.");
    console.log("remove_account      Opens a dialog for removing the currently logged in account from the library system.");
    console.log("logout              Logs out the currently logged in user.");
}

//F1.1
let username="";
let password="";

function printSignup(){
    console.log("Creating a new user account.");
    console.log("Insert your name.");
    username=prompt();
    console.log("Insert new password.");
    password=prompt();
    console.log("Re-enter your password to ensure it matches the one given above.");
    let checkPassword=prompt();
    while (password!==checkPassword) {
        console.log("Passwords do not match.");
        console.log("Insert new password.");
        password=prompt();
        console.log("Re-enter your password to ensure it matches the one given above.");
        checkPassword=prompt();
    }
    console.log("Passwords match.");
    console.log("Your account is now created.");
    getRandom();
    console.log(`Your account id is ${userID}`);
    console.log("Store your account ID in a safe place, preferably in a password manager.");
    console.log("Use the command 'login' to log in to your account.");
    newUser={
        name:username,
        password: password,
        id: userID,
        books: [],
        books_history: []
    };
    users.push(newUser);
    const alteredDataUsers=JSON.stringify(users);
    fs.writeFileSync("users.json", alteredDataUsers,"utf8", (err) => {
        if (err) {
            console.log("Could not save users to file!");
        }
    });
    return users;
    }
    

//F1.2
let i=0;
let user=users[i];
let loggedUser={};
let currentUser={};
function passwordCheck(currentUser){
    let inputPassword=prompt();
    while (currentUser.password!==inputPassword){
        console.log("Wrong password, try again.");
        inputPassword=prompt();
    }
}
function printLogin(){
    console.log("Type your account ID to log in.");
    let inputID=prompt();
    function correctLogin(){
        while (user.id!==inputID) {
            i++;
            user=users[i];
            if ((i===users.length-1)&&(user.id!==inputID)){
                console.log("An account with that ID does not exist. Try again.");
                inputID=prompt();
                i=-1;
                user=users[0];
                correctLogin();
            }
        }
    }
    correctLogin();
    loggedUser=user.id;
    currentUser=user;
    console.log("Account found! Insert your password.");
    passwordCheck(currentUser);
    /*let inputPassword=prompt();
    while (currentUser.password!==inputPassword){
        console.log("Wrong password, try again.");
        inputPassword=prompt();
    } 
    */
    console.log(`Welcome, ${currentUser.name}`);
    return currentUser;
}
//F1.3
function logout(){
    currentUser={};
    loggedUser={};
    console.log("You've logged out");
    return currentUser;
}
//F1.4
function changeName(){
    console.log("Changing the name associated with your account!");
    console.log("Which name should we change your name to?");
    let newName=prompt();
    console.log(`We will address you as ${newName} from now on.`);
    return currentUser.name=newName;
}

//F1.5
function removeAccount(){
    console.log("Do you want to remove this account from the library system? (y/n)");
    let answer=prompt();
    if (answer==="y"){
        console.log("Confirm by giving your password.");
        passwordCheck(currentUser);
        console.log("Are you really really sure??? (y/n)");
        answer=prompt();
        if (answer==="y"){
            console.log("Removed account successfully!");
            let index = users.findIndex(el => el === currentUser);
            users.splice(index, 1);
            console.log(users);
        } else {
            printHelp();
            command= prompt();}
    } else {
        printHelp();
        command= prompt();}
    return users;
}
//F2.1
function list(){
    for (let i=0;i<books.length;i++){
        let book=books[i];
            for (let i=0;i<book.copies.length;i++){
            let copy=book.copies[i];
            if (copy.borrower_id===currentUser.id){
                let borrowedBook=book;
                console.log("Books you've borrowed:");
                console.log(`${book.title} by ${book.author} (${book.published.substring(0, 4)})`);
                console.log(`Due ${copy.due_date}`);
            };
        }
    }
}

//function borrowBook(){}



let running=true;
let command= prompt();
while (running){
    if (command==="help") {printHelp(); command= prompt();}
    if (command==="signup") {printSignup(); command= prompt();};
    if (command==="login") {printLogin(); command= prompt();};
    if (command==="logout") {logout(); command= prompt();};
    if (command==="change_name") {changeName(); command= prompt();};
    if ((command==="remove_account")&&(currentUser!=={})&&(currentUser.books===[])) {
        removeAccount(); 
        command= prompt();
    };
    if ((command==="remove_account")&&(currentUser!=={})&&(currentUser.books!==[])) {
            console.log("Return your books first!"); 
            command= prompt();
    };
    if ((command==="remove_account")&&(currentUser==={})) {
        console.log("Use the command 'login' to log in to your account.");
        command= prompt();
    };
    if (command==="list") {list(); command= prompt();};
    //if (command==="borrow_book") {borrowBook(); command= prompt();};

    if ((command!=="help")||(command!=="signup")||((command!=="login"))||
    (command!=="logout")||(command!=="quit")||(command!=="change_name")||(command!=="list")){printHelp(); command= prompt();}
    if (command==="quit") {
        running=false;
        process.exit();
        //или если программа закончится
        };
}


