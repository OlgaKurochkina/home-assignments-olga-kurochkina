const month = "May";

switch (month) {
    case "January":
        console.log("There are 31 days in January");
        break;
    case "February":
        console.log("There are 28/29 days in February");
        break;
    case "March":
        console.log("There are 31 days in March");
        break;
    case "April":
        console.log("There are 30 days in April");
        break;
    case "May":
        console.log("There are 31 days in May");
        break;
    case "June":
        console.log("There are 30 days in June");
        break; 
    case "July":
        console.log("There are 31 days in July");
        break;
    case "August":
        console.log("There are 31 days in August");
        break;
    case "September":
        console.log("There are 30 days in September");
        break; 
    case "October":
        console.log("There are 31 days in October");
        break;
    case "November":
        console.log("There are 30 days in November");
        break;
    case "December":
        console.log("There are 31 days in December");
        break;
    default:
        console.log("Unknown month");
        break;
}
