const Greeting = (props) => {
    return (
       <div>
         <h2>My name is {props.name} and I am {props.age} years old!</h2>
       </div>
     )
   }

export default Greeting