//Assignment 1 (Types)

console.log("Assignment 1 (Types)");
const appleCount = 13;
const bananaCount = 5;
console.log("Apples: " + appleCount);
console.log("Bananas: " + bananaCount);
console.log("Fruits in total: " + (appleCount + bananaCount));
console.log("\n");

//Assignment 2 (Beware of type coercion)
//a)
console.log("Assignment 2 (Beware of type coercion)");
let number;  // undefined
const result1 = 10 + number;//NaN, because 'number' is undefined
console.log(number, result1);
number = null;
const result2 = 10 + number;  //10, because 10 is before the addition
console.log(result2);


//b)
const a = true;
const b = false;

const c = a + b;  //1
const d = 10 + a;  //11
const e = 10 + b;  //10
console.log(c);
console.log(d);
console.log(e);
/* 
Why are d and e different from each other?"
Because true conversed to number type as 1, false - as 0
*/
console.log("\n");

//Assignment 3 (Comparison)

console.log("Assignment 3 (Comparison)");
const person1Age = 15;
const person2Age = 24;
const isFirstPersonOlder = person1Age > person2Age;
// a) isFirstPersonOlder - false
// b) boolean
// c)

grades1=[
    9,
    6,
    9
]
average1=(grades1[0]+grades1[1]+grades1[2])/grades1.length;  //8
grades2=[
    7,
    10,
    5
]
average2=(grades2[0]+grades2[1]+grades2[2])/grades2.length; //7.333
const ifGrades1Better=average1>average2;
console.log("the 1 class got a higher average score than the 2 class", ifGrades1Better);
console.log("\n");


//Assignment 4 (Fix broken code)

console.log("Assignment 4 (Fix broken code)");

const tree = { x: 6, y: 7, hitpoints: 30 }
const rock = { x: 3, y : 11, hitpoints: 90 }
const damage = 15;
{
    let hitpoints = tree.hitpoints;
    treeHitpointsLeft = hitpoints - damage;
    console.log("Tree hitpoints left: " + treeHitpointsLeft);
}

{
    /*let treeHitpointsLeft;
    let rockHitpointsLeft;*/

    hitpoints = rock.hitpoints;
    rockHitpointsLeft = hitpoints - damage;
    console.log("Rock hitpoints left: " + rockHitpointsLeft);

    
}

console.log("\n");

//Assignment 5 (Arrays)

console.log("Assignment 5 (Arrays)");
age=[
    20, 
    35, 
    27,
    44
]
averageAge=(age[0]+age[1]+age[2]+age[3])/age.length; 
console.log(age);
console.log("The average age of the students", averageAge);
console.log("\n");

//Assignment 6 (Objects)
console.log("Assignment 6 (Objects)");

const book1={
    name: "Dune",
    pageCount: 412,
    read: true,
  }
const book2={
    name: "The Eye of the World",
    pageCount: 782,
    read: false,
  }

console.log(book1);
console.log(book2);
book1.read=false;
book2.read=true;
console.log(book1);
console.log(book2);

console.log("\n");
console.log("EXTRA");


const books=[
    {
    name: "Dune",
    pageCount: 412,
    read: true,
    },
    {
    name: "The Eye of the World",
    pageCount: 782,
    read: false,
    }
]
console.log(books);
books[0].read=false;
books[1].read=true;
console.log(books);


 
 