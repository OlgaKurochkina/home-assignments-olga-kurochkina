import axios from "axios";
import { useState, useEffect } from "react";

const Lecture21 = () => {
  	const [defaultTodos, setTodos] = useState([]);
	useEffect(() => {
		console.log("fetching data!");
		axios.get("http://localhost:3000/defaultTodos")
		.then((response) => {
			console.log(response);
			setTodos(response.data);
		});
	}, []);

    const deleteTodo = (id) => {
		axios.delete(`http://localhost:3000/defaultTodos/${id}`)
		.then((response) => {
			setTodos(defaultTodos.filter((todo) => todo.id !== id));
		}).catch((response)=> {
			console.log("eipä onnistnu")
		})
	}

    const postTodo = () => {
		axios.post("http://localhost:3000/defaultTodos", 
			{ text: "Read a book" })
			.then((response) => {
				setTodos([...defaultTodos,response.data]);
			})
	}

    const changeTodo= (id) => {
		const todo = defaultTodos.find((todo) => todo.id === id);
		const changedTodo = {...todo, text: "new text"};
		axios.put(`http://localhost:3000/defaultTodos/${id}`, changedTodo)
		.then((response) => {
			setTodos(defaultTodos.map(todo => todo.id === id ? response.data : todo));
		})
	}



	const styleGreen = { backgroundColor: "#c3f400", maxWidth: 300, margin: "auto",};
    const buttonStyle = { padding: 5, margin: 5 };
    
	return (
		<div>
			{defaultTodos.map((todo) => (
				<div style={styleGreen} key={todo.id}>
					<h3>{todo.text}</h3>
                    <button style={buttonStyle} onClick={()=>deleteTodo(todo.id)}>X</button>
                    <button style={buttonStyle} onClick={()=>changeTodo(todo.id)}>Change</button>
					<br/>
				</div>)
			)}
            
            <button onClick={postTodo}>Add a new note!</button>
		</div>
	)
}

export default Lecture21;
