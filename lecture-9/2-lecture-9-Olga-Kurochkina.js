function numberRange(start,end){
    let array=[];
    if (start<end){
        let f=end-start;
        for (let i=0; i<=f; i++) {
            array.push(start);
            start++;
        }
    } else {
        let f=start-end;
        for (let i=0; i<=f; i++){
            array.push(start);
            start--;
        }
    }
    return array;
}
console.log(numberRange(1, 5));   // prints [ 1, 2, 3, 4, 5 ]
console.log(numberRange(-5, -1)); // prints [ -5, -4, -3, -2, -1 ]
console.log(numberRange(9, 5));  // prints [ 9, 8, 7, 6, 5 ]
