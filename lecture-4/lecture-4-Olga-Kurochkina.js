//Assignment 1 (Variables)
let value = 30;
const multiplier = 1.2;
value = value * multiplier;
console.log("Assignment 1");
console.log(value);

//Assignment 2 (Discounted price)

const price=1000;
const discountInPercents=30;
result=price*((100-discountInPercents)/100);
console.log("Assignment 2");
console.log ("Original price ", price);
console.log ("Discount ", discountInPercents, "% " );
console.log ("Final price", result);

//Assignment 3 (Travel time)

const distance=120;
const speed=25;
timeH=Math.floor(distance/speed);
timeM=((distance/speed*10-timeH*10)*6);
/*
I multiplied by 10 to avoid a floating point inaccuracy
When I wrote timeM=((distance/speed-timeH)*60)
timeM was not 47.99999 (not 48) 
*/
console.log("Assignment 3");
console.log ("Distance", distance, "km");
console.log ("Speed ", speed, "km/h" );
console.log ("Time", timeH, "h", timeM, "m");

//Assignment 4 (Seconds in a year)

const days=365;
const daysInALeapYear=366;
const hours=24;
const minutes=60;
const seconds=60;
SecondsInAYear=days*hours*minutes*seconds;
SecondsInALeapYear=daysInALeapYear*hours*minutes*seconds;
console.log("Assignment 4");
console.log (SecondsInAYear, "seconds in a year");
console.log (SecondsInALeapYear, "seconds in a leap year");

//Assignment 5 (Average grade)

const grade1 = 8;
const grade2 = 10;
const grade3 = 7;
const gradeCount = 3;
const averageGrade = (grade1 + grade2 + grade3)/gradeCount;
console.log("Assignment 5");
console.log(averageGrade);


//Assignment 6 (Area of a square)

const lengthOfASquare=8; 
areaSquare=lengthOfASquare**2;
console.log("Assignment 6");
console.log("The area of a square is "+areaSquare);

//Assignment 7 (Area of a rectangle)

const length=8; 
const width=5;
areaRectangle=length*width;
areaTriangle=0.5*length*width;
console.log("Assignment 7");
console.log("The area of a rectangle is "+areaRectangle);
console.log("The area of a triangle is "+areaTriangle);


// Assignment 8 (Division)

let candies=100;
let colleagues=5;
let sweettooths=++colleagues;
addCandies=candies%sweettooths;
candies-=addCandies;
candiesForCollegues=candies/sweettooths;
myCandies=candiesForCollegues+addCandies;
//candies % sweettooths;
console.log("Assignment 8");
console.log("Each colleague receives", candiesForCollegues, "candies");
console.log("I receive", myCandies, "candies");


//Assignment 9 (Exponentiation, advanced)

let income1=90;
let income2=75;
let exponent=0.9;
difference=income1-income2; 
console.log("Assignment 9");
console.log("Difference "+difference);
income1**=exponent;
income2**=exponent;
difference=income1-income2;
console.log("New difference "+difference);


