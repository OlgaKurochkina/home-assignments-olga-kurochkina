class Robot {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    handleCommandList(commandList) {
        for (let i = 0; i < commandList.length; i++) {
            let letter = commandList.charAt(i);
            if (letter === "N") { this.y++; };
            if (letter === "E") { this.x++; };
            if (letter === "S") { this.y--; };
            if (letter === "W") { this.x--; };
            if (letter === "C") { };
            if (letter === "B") { break };
        }
        return this.x;
        return this.y;
    }
}

const robot = new Robot(0, 0);
console.log(robot);
robot.handleCommandList("NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE");
console.log(robot);



