class Room {
    constructor(width, height) {
        this.width = width;
        this.height = height;
    }
    getArea() {
        return this.area = this.height * this.width;
    }
    addFurniture(string) {
        return this.furniture.push(string);
    }
}

const room = new Room(4.5, 6.0);
console.log(room); // Room { width: 4.5, height: 6 }

const area = room.getArea();
console.log(area); // prints 27

room.furniture = [];
room.addFurniture("sofa");
room.addFurniture("bed");
room.addFurniture("chair");
console.log(room);

