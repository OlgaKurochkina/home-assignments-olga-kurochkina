//a)

function sum(limit){
    let result=0;
    while (limit>0){
        limit--;
        result=result+limit;
    }
    return result;
}
console.log(sum(10)); // prints 45 (= 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9)*/

//b)
const b= new Promise (function (resolve,reject){
    setTimeout(() => {
        resolve(result=sum(50000))
    },0)
})
b.then (() => {
    console.log(result)
})

//c)
const p= new Promise (function (resolve,reject){
    setTimeout(() => {
        resolve(result=sum(50000))
    },2000)
})
p.then (() => {
    console.log(result)
})

//d)
function createDelayedCalculation(limit, milliseconds){
const d= new Promise (function (resolve,reject){
        setTimeout(() => {
            resolve(result=sum(limit))
        },milliseconds)
    })
d.then (() => {
        console.log(result)
    })
}
createDelayedCalculation(20000000, 2000);
createDelayedCalculation(50000, 500)

//e)
//the function with the least delay is executed first, since she has been in the queue for less time


// Prints 199999990000000 after a delay of 2 seconds
//createDelayedCalculation(20000000, 2000).then(result => console.log(result));

// Prints 1249975000 after a delay of 0.5 seconds
//createDelayedCalculation(50000, 500).then(result => console.log(result));
