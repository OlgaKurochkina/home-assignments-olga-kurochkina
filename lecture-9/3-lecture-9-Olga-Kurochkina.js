function insertNumber(array, number){
    let insertPosition=0;
    for (let i=0; i<array.length;i++){
        if ((array[i]<=number)){
            insertPosition=i+1
        } else {break};
    }
    array.splice(insertPosition,0,number);
    return array;
}
const array = [ 1, 3, 4, 7, 11 ];
console.log(array);
insertNumber(array, 8);
console.log(array); // prints [ 1, 3, 4, 7, 8, 11 ] 

