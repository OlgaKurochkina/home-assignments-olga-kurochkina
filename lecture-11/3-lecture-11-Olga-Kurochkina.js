const charIndex = {
    a: 0, b: 1, c: 2, d: 3, e: 4, f: 5, g: 6, h: 7, i: 8, j: 9, k: 10, l: 11,
    m: 12, n: 13, o: 14, p: 15, q: 16, r: 17, s: 18, t: 19, u: 20, v: 21, w: 22, x: 23, y: 24, z: 25
};
let newObject = {};

function getCountOfLetters(string) {
    for (let i = 0; Object.keys(charIndex).length > i; i++) { //create a new object with alphabet and zeros
        let letter = Object.keys(charIndex)[i];
        newObject[letter] = 0;
    }
    for (let i = 0; string.length > i; i++) { //count the number of repetitions of letters
        let letter = string.charAt(i);
        if (!(letter === " ")) {
            newObject[letter] += 1;
        }
    }
    for (let i = 0; Object.keys(charIndex).length > i; i++) { // remove elements with value 0
        let letter = Object.keys(charIndex)[i];
        if (newObject[letter] === 0) {
            delete newObject[letter];
        }
    }
    return newObject;
}
const result = getCountOfLetters("a black cat");
console.log(result); // prints {a: 3, b: 1, c: 2, k: 1, l: 1, t: 1}