function reverseWords(sentence){
    let newString ="";
    let newWord="";
    let firstLetter=0;
    for (let i=0; i<sentence.length;i++){
        let letter=sentence.charAt(i);
        if (!(letter===" ")) {
            newWord=letter+newWord;
        }
        if ((letter===" ")||(i===(sentence.length-1))){
            newString+=newWord;
            newWord="";
        } 
        if (letter===" "){newString+=" "};  
    }
    return newString;
}
const sentence = "this is a short sentence";
const reversed = reverseWords(sentence);
console.log(reversed); // prints "siht si a trohs ecnetnes"
