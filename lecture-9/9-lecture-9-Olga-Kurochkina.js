const charIndex = {a:1,b:2,c:3,d:4,e:5,f:6,g:7,h:8,i:9,j:10,k:11,l:12,
m:13,n:14,o:15,p:16,q:17,r:18,s:19,t:20,u:21,v:22,w:23,x:24,y:25,z:26};
let number="";

function charIndexString(word){
    let newWord="";
    for (i=0;word.length>i;i++){
        let letter=word.charAt(i);
        let number=charIndex[letter];
        newWord+=number;
    }
    return newWord;
}
console.log(charIndexString("bead")); // prints "2514"
console.log(charIndexString("rose")); // prints "1815195"
