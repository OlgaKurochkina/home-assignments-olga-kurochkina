let x = 0;
let y = 0;
const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";
const commandHandlers = {
    N: () => y++,
    E: () => x++,
    S: () => y--,
    W: () => x--,
    C: () => { },
}
function handleCommandList(commandList) {
    for (let i = 0; i < commandList.length; i++) {
        let letter = commandList.charAt(i);
        if (letter === "B") {
            break;
        } else {
            commandHandlers[letter]();
        }
    }
    return x;
    return y;
}
handleCommandList(commandList);
console.log(`Final robot position, x: ${x}, y: ${y}`);
