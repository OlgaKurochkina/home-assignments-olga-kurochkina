import { useState, useEffect } from "react"
import TodoNote from './TodoNote'
import Lecture21 from './Lecture21'
import InputForm from './InputForm'
import { v4 as uuid } from 'uuid';
import axios from 'axios'


const App = () => {

  const [defaultTodos, setTodos]=useState( [
    {id: uuid(), text: 'Buy potatoes', complete: false},
    {id: uuid(), text: 'Make food', complete: false},
    {id: uuid(), text: 'Exercise', complete: false},
    {id: uuid(), text: 'Do the dishes', complete: false},
    {id: uuid(), text: 'Floss the teeth', complete: false},
    {id: uuid(), text: 'Play videogames', complete: true},
]);
const toggleCompletion = (id) => {
  const newArray = defaultTodos.map((todo,index) => 
    (index === id 
      ? {
        ...todo,
        complete: !(todo.complete)
      } 
      : todo)
  )
  setTodos(newArray);
}
const  removeTodo = (id) => {
  const filteredTodos = defaultTodos.filter(
    (todo) => todo.id !== id
  )
  setTodos(filteredTodos);
}
const [searchQuery, setSearchQuery] = useState("");
	
const displayedTodos = (searchQuery==="")? defaultTodos : defaultTodos.filter(todo =>
  todo.text.toLowerCase().includes(searchQuery.toLowerCase())
);

const addTodo=(text)=>
  {setTodos([...defaultTodos,  {id: uuid(), text: text, complete: false},])}

const editTodoText=(id, newText)=>{
  const editedArray = defaultTodos.map((todo,index) => 
  (index === id 
    ? {
      ...todo,
      text: newText
    } 
    : todo)
)
setTodos(editedArray);
  }

return ( 
  <>
  <div>
    <Lecture21/>
    <br></br>
  <fieldset>
			<legend>Searching for notes</legend>			     
			<input
        placeholder="Search"
				value = {searchQuery}
				onChange = {(event) => {
					setSearchQuery(event.target.value);
				}}
			/>
           </fieldset>
      {displayedTodos.map((todo,index) => 
          <div>
              <TodoNote 
                todo={todo} 
                index={index}
                onCompletionToggle ={
                  () => 
                    toggleCompletion(index)
                  }
                  onRemoveClick = {
                    () => 
                      removeTodo(todo.id)
                    }
                  onChange = {editTodoText}/>
          </div>
          
      )}
      <InputForm  onSubmit = {addTodo}/>
  </div>
  </>
)
}	
export default App

