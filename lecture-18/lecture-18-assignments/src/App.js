import { useState } from "react";
import "./App.css"

const Assignment1 = () => {
  const [pressed, setPressed] = useState(true);
  const button1=()=>{
    (pressed===true)?  setPressed(false): setPressed(true);
	}
	return (
    <div>
		<button onClick={button1}>Toggle</button>
    <p className={(pressed)? "text" : "hidden"}>Fear is the path to the darkside</p>
    </div>
	)
}

const DisappearingButton = ({number}) => {
  const [hasBeenPressed, setHasBeenPressed] = useState(false);
	const onClickFunc = () => {
		(hasBeenPressed===true)?  setHasBeenPressed(false): setHasBeenPressed(true);
	}
	return (
		<>
      {!hasBeenPressed && <button onClick={onClickFunc}>Button {number}</button>}
    </>
	)
}

const Assignment3 = (props) => {
  const [hasBeenPressed, setHasBeenPressed] = useState(false);
	const onClickFunc = () => {
		setHasBeenPressed(true);
	}
	return (
    <div>
    {props.props.map(name => 
    <table>
		<button onClick={onClickFunc} className={
			(hasBeenPressed)
			? "green-button"
			: "red-button"
		}>
			{name}
		</button>
    </table>
    )}
    </div>
	)
}

const Buttons = () => {
	return (
		<div className="buttons">
			<br/>
			<DisappearingButton number={1}/>
			<DisappearingButton number={2}/>
			<DisappearingButton number={3}/>
			<DisappearingButton number={4}/>
			<DisappearingButton number={5}/>
		</div>
	)
}

const App = () => {
  const names = [
    "Anakin Skywalker",
    "Leia Organa",
    "Han Solo",
    "C-3PO",
    "R2-D2",
    "Darth Vader",
    "Obi-Wan Kenobi",
    "Yoda",
    "Palpatine",
    "Boba Fett",
    "Lando Calrissian",
    "Jabba the Hutt",
    "Mace Windu",
    "Padmé Amidala",
    "Count Dooku",
    "Qui-Gon Jinn",
    "Aayla Secura",
    "Ahsoka Tano",
    "Ki-Adi-Mundi",
    "Luminara Unduli",
    "Plo Koon",
    "Kit Fisto",
    "Shmi Skywalker",
    "Beru Whitesun",
    "Owen Lars"
  ];
  
return (
  <div>
    <h1>Assignment 1</h1>
    <br/><Assignment1/>  
    <h1>Assignment 2</h1>
    <br/><Buttons/> 
    <h1>Assignment 3</h1>
    <br/><Assignment3 props={names}/>  
  </div>
);
}
export default App
