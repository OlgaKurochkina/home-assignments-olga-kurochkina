function sortNumberArray(array){
   let number=array[0];
   let temporary=0;
   for (a=0;a<(array.length-1);a++){
      for (i=0;i<array.length;i++){
         if (array[i]>array[i+1]) {
            temporary=array[i];
            array[i]=array[i+1];
            array[i+1]=temporary;
         }
      }
   }
   return array;
}
const array = [ 4, 19, 7, 1, 9, 22, 6, 13 ];
console.log(array);
sortNumberArray(array);
console.log(array); // prints [ 1, 4, 6, 7, 9, 13, 19, 22 ]
const array2 = [20, 19, 7, 6, 5, 3, 2, 1 ];
console.log(array2);
sortNumberArray(array2);
console.log(array2); // prints [1,2,3,5,6,7,19,20]¨

