function sentencify (array, number){
    if ((number+1)>=array.length) {return array[number]+"!"} 
    else {
    return array[number]+' '+sentencify(array,number+1)
    }
}
const wordArray = [ "The", "quick", "silver", "wolf" ];
console.log(sentencify(wordArray, 0)); // prints "The quick silver wolf!"
console.log(sentencify(wordArray, 1)); // prints "quick silver wolf!"
